<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class m_wilayah extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function allProvinsi($id='')
  {
    $this->db->select('*');
    $this->db->from('ref_provinsi');
    if (isset($id) && $id != '') {
      $this->db->where('id',$id);
    }
    $query = $this->db->get();
    return $query->result();
  }

  public function allKabupaten($id='')
  {
    $this->db->select('*');
    $this->db->from('ref_kabupaten');
    if (isset($id) && $id != '') {
      $this->db->where('provinsi_id',$id);
    }
    $query = $this->db->get();
    return $query->result();
  }

  public function allKecamatan($id='')
  {
    $this->db->select('*');
    $this->db->from('ref_kecamatan');
    if (isset($id) && $id != '') {
      $this->db->where('kabkot_id',$id);
    }
    $query = $this->db->get();
    return $query->result();
  }

  public function allKelurahan($id='')
  {
    $this->db->select('*');
    $this->db->from('ref_kelurahan');
    if (isset($id) && $id != '') {
      $this->db->where('kecamatan_id',$id);
    }
    $query = $this->db->get();
    return $query->result();
  }

  public function allRw($id)
  {
    $this->db->select('*');
    $this->db->from('ref_rukun_warga');
    if (isset($id) && $id != '') {
      $this->db->where('kelurahan_id',$id);
    }
    $query = $this->db->get();
    return $query->result();
  }

  public function allRt($id='')
  {
    $this->db->select('*');
    $this->db->from('ref_rukun_tetangga');
    if (isset($id) && $id != '') {
      $this->db->where('rw_id',$id);
    }
    $query = $this->db->get();
    return $query->result();
  }

  public function allWarga($id='')
  {
    $this->db->select('*');
    $this->db->from('ref_data_warga');
    if (isset($id) && $id != '') {
      $this->db->where('id_rt',$id);
    }
    $query = $this->db->get();
    return $query->result();
  }

  public function Wherewilayah($find)
  {
    $this->db->select('
     rp.id as id_prv,
     rp.provinsi,
     rka.id as id_rka,
     rka.kabupaten_kota,
     rka.ibukota,
     rke.id as id_kec,
     rke.kecamatan,
     rkl.id as id_rkl,
     rkl.kelurahan,
     rkl.kd_pos,
     rrw.id as id_rw,
     rrw.no_rw,
     rrw.nama_ketua_rw
    ');
    $this->db->join('ref_kelurahan rkl','rrw.kelurahan_id=rkl.id');
    $this->db->join('ref_kecamatan rke','rkl.kecamatan_id=rke.id');
    $this->db->join('ref_kabupaten rka','rke.kabkot_id=rka.id');
    $this->db->join('ref_provinsi rp','rka.provinsi_id=rp.id');
    return $this->db->get_where('ref_rukun_warga rrw', $find)->row();

  }

  public function allKabupatenJml($id)
  {
    $this->db->group_by('rk.id');
    $this->db->select('rk.*, COUNT(rdw.id) as jumlah');
    $this->db->join('ref_data_warga rdw','rk.id = rdw.id_kab', 'left');
    return $this->db->get_where('ref_kabupaten rk', ['rk.provinsi_id' => $id])->result();
  }

  public function allKecamatanJml($id)
  {
    $this->db->group_by('rk.id');
    $this->db->select('rk.*, COUNT(rdw.id) as jumlah');
    $this->db->join('ref_data_warga rdw','rk.id = rdw.id_kec', 'left');
    return $this->db->get_where('ref_kecamatan rk', ['rk.kabkot_id' => $id])->result();
  }

  public function allKelurahanJml($id)
  {
    $this->db->group_by('rk.id');
    $this->db->select('rk.*, COUNT(rdw.id) as jumlah');
    $this->db->join('ref_data_warga rdw','rk.id = rdw.id_kel', 'left');
    return $this->db->get_where('ref_kelurahan rk', ['rk.kecamatan_id' => $id])->result();
  }

  public function allRwJml($id)
  {
    $this->db->group_by('rk.id');
    $this->db->select('rk.*, COUNT(rdw.id) as jumlah');
    $this->db->join('ref_data_warga rdw','rk.id = rdw.id_rw', 'left');
    return $this->db->get_where('ref_rukun_warga rk', ['rk.kelurahan_id' => $id])->result();
  }

  public function allRtJml($id)
  {
    $this->db->group_by('rk.id');
    $this->db->select('rk.*, COUNT(rdw.id) as jumlah');
    $this->db->join('ref_data_warga rdw','rk.id = rdw.id_rt', 'left');
    return $this->db->get_where('ref_rukun_tetangga rk', ['rk.rw_id' => $id])->result();
  }
}

?>
