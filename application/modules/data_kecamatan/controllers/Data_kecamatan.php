<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class Data_kecamatan extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->isLogin();
    $this->load->library('encrypt');
    $this->load->library('form_validation');
    $this->load->model('m_kecamatan');
    // $this->load->model('m_wilayah');
  }

  public function isLogin()
  {
    if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('loginapp');
		}
  }

  public function index($value='')
  {
    $data['sub_judul_form']="Data Kecamatan";
    $data['breadcrumbs'] = array(
      array (
        'link' => 'welcome',
        'name' => 'Home'
        ),
      array (
        'link' => 'data_kecamatan',
        'name' => 'Kecamatan'
        )
      );
    $data['role'] = get_role($this->session->userdata('sesi_id'));
    $this->template->load('template_frontend','v_index', $data);
  }

  public function ajax_list($value='')
  {
    $list = $this->m_kecamatan->get_datatables();

    $data = array();

    $no = $_POST['start'];
    foreach ($list as $datas) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $datas->kabupaten_kota;
      $row[] = $datas->ibukota;
      $row[] = $datas->kecamatan;
      $row[] = anchor('data_kecamatan/edit/'.$this->encrypt->encode($datas->id), '<i class="fa fa-pencil"></i> Ubah', array('class' => 'btn btn-warning btn-sm'));
      $data[] = $row;
    }
    $output = array(
      'draw' => $_POST['draw'],
      'recordsTotal' => $this->m_kecamatan->count_all(),
      'recordsFiltered' => $this->m_kecamatan->count_filtered(),
      'data' => $data
    );

    echo json_encode($output);
  }

  public function add()
  {
    $valid = $this->form_validation;
    $valid->set_rules('nama_kotakab','nama_kotakab','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('kecamatan','kecamatan','required','xss_clean',array('required'=> '%s Harus Diisi',));

    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Kecamatan";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'data_kecamatan',
          'name' => 'Kecamatan'
        ),
        array (
          'link' => 'data_kecamatan/add',
          'name' => 'Tambah Kecamatan'
        )
      );
      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_add', $data);
    } else {
      $id_kabkot  = htmlentities($this->input->post('nama_kotakab'), ENT_QUOTES, 'UTF-8');
      $kecamatan  = htmlentities($this->input->post('kecamatan'), ENT_QUOTES, 'UTF-8');

      $data['kabkot_id'] = $id_kabkot;
      $data['kecamatan'] = $kecamatan;
      $this->m_kecamtan->tambah($data);
      $this->session->set_flashdata('message_sukses','Data Berhasil Ditambah');
      redirect(base_url('data_kecamatan/add'),'refresh');
    }

  }

  public function edit($id)
  {
    $kode = $this->encrypt->decode($id);

    $valid = $this->form_validation;
    $valid->set_rules('nama_kotakab','nama_kotakab','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('kecamatan','kecamatan','required','xss_clean',array('required'=> '%s Harus Diisi',));

    $id_kabkot  = htmlentities($this->input->post('nama_kotakab'), ENT_QUOTES, 'UTF-8');
    $kecamatan  = htmlentities($this->input->post('kecamatan'), ENT_QUOTES, 'UTF-8');

    $dataz = $this->m_kecamatan->detail($kode);
    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Kecamatan";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'data_kecamatan',
          'name' => 'Kecamatan'
        ),
        array (
          'link' => 'data_kecamatan/edit/'.$id,
          'name' => 'Edit Kecamatan'
        )
      );
      $data['kode'] = $id;
      $data['isi'] = $dataz;
      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_edit', $data);
    } else {
      $datax['kabkot_id'] = $id_kabkot;
      $datax['kecamatan'] = $kecamatan;

      $this->m_kecamatan->edit($kode,$datax);
      $this->session->set_flashdata('message_sukses','Data Berhasil Diupdate');
      redirect(base_url('data_kecamatan/edit/'.$id),'refresh');
    }

  }

  // link : http://localhost/monitoring_wilayah/data_kecamatan/generate
  // public function generate($value='')
  // {
  //   $prv = $this->m_wilayah->allKecamatan();
  //   foreach ($prv as $key) {
  //     $name = explode(" ",$key->kecamatan); $set = '';
  //     for ($i=0; $i < count($name); $i++) {
  //       if ($i > count($name)) { $set .= $name[$i].'_'; }
  //       else { $set .= $name[$i]; }
  //     }
  //     $data['username']       = 'admin_'.strtolower($set);
  //     $data['password']       = sha1(md5('123456'));
  //     $data['nama_lengkap']   = $key->kecamatan;
  //     $data['create_time']    = date('Y-m-d H:i:s');
  //     $data['create_by']      = $this->session->userdata('sesi_nama_lengkap');
  //     $data['status_aktif']   = 'Y';
  //     $data['id_user_group']  = 4;
  //     $data['id_wilayah']     = $key->id;
  //     $xss_data = $this->security->xss_clean($data);
  //     $this->db->insert('ref_users', $xss_data);
  //   }
  // }

}

?>
