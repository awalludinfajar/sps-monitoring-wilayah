<div class="page-header">
	<div class="page-header-content" style="padding:0;">
		<div class="page-title" style="padding-top:0; padding-bottom:15px;">
			<h4>
				<i class="icon-arrow-left52 position-left"></i>
				<span class="text-semibold"><?php echo $sub_judul_form;?></span>
			</h4>
			<ul class="breadcrumb breadcrumb-caret position-right">
				<?php foreach ($breadcrumbs as $key => $value) { ?>
				<li>
					<a href=<?php echo site_url($value['link'])?> > <?php echo $value['name']; ?></a>
					<?php echo (count($breadcrumbs)-1)==$key?"":""; ?>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>

<div class="panel panel-flat">
  <div class = "panel-heading">
    <legend class="text-semibold">Edit RT</legend>
    <div class="panel-body" style="padding:0;">
      <?php echo form_open('rukun_tetangga/edit/'.$kode, array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>
        <?php
        if ($this->session->flashdata('message_gagal')) {
          echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
          echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
        }?>
        <div class="form-group">
    			<label class="col-lg-1 control-label">Nama Provinsi</label>
    			<div class="col-lg-6">
            <select data-rule-required="true" name="nama_provinsi" class="select-search" id="nama_provinsi"></select>
    			</div>
    		</div>
        <div class="form-group">
    			<label class="col-lg-1 control-label">Nama Kota/Kabupaten</label>
    			<div class="col-lg-6">
            <select data-rule-required="true" name="nama_kotakab" class="select-search" id="nama_kotakab"></select>
    			</div>
    		</div>
        <div class="form-group">
    			<label class="col-lg-1 control-label">Nama Kecamatan</label>
    			<div class="col-lg-6">
						<select data-rule-required="true" name="kecamatan_id" class="select-search" id="kecamatan_id"></select>
    			</div>
    		</div>
        <div class="form-group">
    			<label class="col-lg-1 control-label">Nama Kelurahan</label>
    			<div class="col-lg-6">
						<select data-rule-required="true" name="kelurahan_id" class="select-search" id="kelurahan_id"></select>
    			</div>
    		</div>
        <div class="form-group">
    			<label class="col-lg-1 control-label">No.RW / Ketua RW</label>
    			<div class="col-lg-6">
						<select data-rule-required="true" name="ketua_rw_id" class="select-search" id="ketua_rw_id"></select>
    			</div>
    		</div>
        <div class="form-group">
    			<label class="col-lg-1 control-label">No RT</label>
    			<div class="col-lg-6">
						<input required placeholder="No RT" type="number" name="no_rt" class="form-control" data-rule-required="true" value="<?php echo $isi['no_rt']; ?>">
    			</div>
    		</div>
        <div class="form-group">
    			<label class="col-lg-1 control-label">Nama Ketua RT</label>
    			<div class="col-lg-6">
						<input required placeholder="Ketua RT" type="text" name="n_ketua_rt" class="form-control" data-rule-required="true" value="<?php echo $isi['nama_ketua_rt']; ?>">
    			</div>
    		</div>
				<div class="form-group">
    			<label class="col-lg-1 control-label">Nomor Hp</label>
    			<div class="col-lg-6">
						<input required placeholder="No.Hp" type="text" name="no_hp_rt" class="form-control" data-rule-required="true" value="<?php echo $isi['no_hp']; ?>">
    			</div>
    		</div>
        <div class="text-right col-lg-7">
    			<button type="submit" class="btn btn-success btn-labeled btn-xs"><b><i class="icon-files-empty2"></i></b> Simpan</button>
    			<a class="btn btn-danger btn-labeled btn-xs"  href="<?php echo site_url();?>rukun_tetangga"><b><i class="icon-arrow-left13"></i></b> Kembali</a>
    		</div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#nama_provinsi').select2();
  $('#nama_kotakab').select2();
  $('#kecamatan_id').select2();
  $('#kelurahan_id').select2();
  $('#ketua_rw_id').select2();
  $(document).ready(function() {
    let url0 = "<?php echo site_url('api/selectKetuaRWSelected/'.$isi['rw_id']); ?>/?ondb=rrw.id";
    $('#ketua_rw_id').load(url0);

    let url1 = "<?php echo site_url('api/selectKelurahanSelected/'.$isi['rw_id']); ?>/?ondb=rrw.id";
    $('#kelurahan_id').load(url1);

    let url2 = "<?php echo site_url('api/selectKecamatanSelected/'.$isi['rw_id']); ?>/?ondb=rrw.id";
		$('#kecamatan_id').load(url2);

		let url3 = "<?php echo site_url('api/selectKabKotaSlected/'.$isi['rw_id']); ?>/?ondb=rrw.id";
		$('#nama_kotakab').load(url3);

		let url4 = "<?php echo site_url('api/selectProvinsiSlected/'.$isi['rw_id']); ?>/?ondb=rrw.id";
		$('#nama_provinsi').load(url4);
  });

  $('#nama_provinsi').change(function() {
    var url = "<?php echo site_url('api/selectKotaKab'); ?>/"+$(this).val();
    $('#nama_kotakab').load(url);
    return false;
  });

  $('#nama_kotakab').change(function() {
    var url = "<?php echo site_url('api/selectKecamatan'); ?>/"+$(this).val();
    $('#kecamatan_id').load(url);1
    return false;
  });

  $('#kecamatan_id').change(function() {
    var url = "<?php echo site_url('api/selectKelurahan'); ?>/"+$(this).val();
    $('#kelurahan_id').load(url);1
    return false;
  });

  $('#kelurahan_id').change(function() {
    var url = "<?php echo site_url('api/selectRkwrga'); ?>/"+$(this).val();
    $('#ketua_rw_id').load(url);
    return false;
  });
</script>
