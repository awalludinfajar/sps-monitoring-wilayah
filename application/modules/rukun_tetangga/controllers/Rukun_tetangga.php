<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class Rukun_tetangga extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->isLogin();
    $this->load->library('encrypt');
    $this->load->library('form_validation');
    $this->load->model('m_rukun_tetangga');
    $this->load->model('m_wilayah');
  }

  public function isLogin()
  {
    if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('loginapp');
		}
  }

  public function index($value='')
  {
    $data['sub_judul_form']="Data Rukun Tetangga (RT)";
    $data['breadcrumbs'] = array(
      array (
        'link' => 'welcome',
        'name' => 'Home'
        ),
      array (
        'link' => 'rukun_tetangga',
        'name' => 'Rukun Tetangga'
        )
      );
    $data['role'] = get_role($this->session->userdata('sesi_id'));
    $this->template->load('template_frontend','v_index', $data);
  }

  public function ajax_list($value='')
  {
    $list = $this->m_rukun_tetangga->get_datatables();

    $data = array();

    $no = $_POST['start'];
    foreach ($list as $datas) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $datas->kelurahan.'('.$datas->kd_pos.')';
      $row[] = $datas->no_rw.' / '.$datas->nama_ketua_rw;
      $row[] = $datas->no_rt.' / '.$datas->nama_ketua_rt;
      $row[] = $datas->no_hp;
      $row[] = anchor('rukun_tetangga/edit/'.$this->encrypt->encode($datas->id), '<i class="fa fa-pencil"></i> Ubah', array('class' => 'btn btn-warning btn-sm'));
      $data[] = $row;
    }
    $output = array(
      'draw' => $_POST['draw'],
      'recordsTotal' => $this->m_rukun_tetangga->count_all(),
      'recordsFiltered' => $this->m_rukun_tetangga->count_filtered(),
      'data' => $data
    );

    echo json_encode($output);
  }

  public function add($value='')
  {
    $valid = $this->form_validation;
    $valid->set_rules('kelurahan_id','kelurahan_id','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('ketua_rw_id','ketua_rw_id','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('no_rt','no_rt','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('n_ketua_rt','n_ketua_rt','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('no_hp_rt','no_hp_rt','required','xss_clean',array('required'=> '%s Harus Diisi',));

    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Rukun Tetangga (RT)";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'rukun_tetangga',
          'name' => 'Rukun Tetangga'
        ),
        array (
          'link' => 'rukun_tetangga/add',
          'name' => 'Tambah RT'
        )
      );

      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_add', $data);
    } else {
      $kelurahan  = htmlentities($this->input->post('kelurahan_id'), ENT_QUOTES, 'UTF-8');
      $ketua_rw   = htmlentities($this->input->post('ketua_rw_id'), ENT_QUOTES, 'UTF-8');
      $no_rt      = htmlentities($this->input->post('no_rt'), ENT_QUOTES, 'UTF-8');
      $n_ketua_rt = htmlentities($this->input->post('n_ketua_rt'), ENT_QUOTES, 'UTF-8');
      $no_hanpon  = htmlentities($this->input->post('no_hp_rt'), ENT_QUOTES, 'UTF-8');

      $data['kelurahan_id']  = $kelurahan;
      $data['rw_id']         = $ketua_rw;
      $data['no_rt']         = $no_rt;
      $data['nama_ketua_rt'] = $n_ketua_rt;
      $data['no_hp']         = $no_hanpon;
      $this->m_rukun_tetangga->tambah($data);
      $this->session->set_flashdata('message_sukses','Data Berhasil Ditambah');
      redirect(base_url('rukun_tetangga/add'),'refresh');
    }

  }

  public function edit($id)
  {
    $kode = $this->encrypt->decode($id);

    $valid = $this->form_validation;
    $valid->set_rules('kelurahan_id','kelurahan_id','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('ketua_rw_id','ketua_rw_id','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('no_rt','no_rt','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('n_ketua_rt','n_ketua_rt','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('no_hp_rt','no_hp_rt','required','xss_clean',array('required'=> '%s Harus Diisi',));

    $kelurahan  = htmlentities($this->input->post('kelurahan_id'), ENT_QUOTES, 'UTF-8');
    $ketua_rw   = htmlentities($this->input->post('ketua_rw_id'), ENT_QUOTES, 'UTF-8');
    $no_rt      = htmlentities($this->input->post('no_rt'), ENT_QUOTES, 'UTF-8');
    $n_ketua_rt = htmlentities($this->input->post('n_ketua_rt'), ENT_QUOTES, 'UTF-8');
    $no_hanpon  = htmlentities($this->input->post('no_hp_rt'), ENT_QUOTES, 'UTF-8');

    $dataz = $this->m_rukun_tetangga->detail($kode);
    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Rukun Tetangga (RT)";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'rukun_tetangga',
          'name' => 'Rukun Tetangga'
        ),
        array (
          'link' => 'rukun_tetangga/edit/'.$id,
          'name' => 'Edit RT'
        )
      );

      $data['kode'] = $id;
      $data['isi'] = $dataz;
      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_edit', $data);
    } else {
      $data['kelurahan_id']  = $kelurahan;
      $data['rw_id']         = $ketua_rw;
      $data['no_rt']         = $no_rt;
      $data['nama_ketua_rt'] = $n_ketua_rt;
      $data['no_hp']         = $no_hanpon;
      $this->m_rukun_tetangga->edit($kode,$data);
      $this->session->set_flashdata('message_sukses','Data Berhasil DiUbah');
      redirect(base_url('rukun_tetangga/edit/'.$id),'refresh');
    }

  }

  // link : http://localhost/monitoring_wilayah/rukun_tetangga/generate
  // public function generate($value='')
  // {
  //   $prv = $this->m_wilayah->allRt();
  //   foreach ($prv as $key) {
  //     $name = explode(" ",$key->nama_ketua_rt); $set = '';
  //     for ($i=0; $i < count($name); $i++) {
  //       if ($i > count($name)) { $set .= $name[$i].'_'; }
  //       else { $set .= $name[$i]; }
  //     }
  //     $data['username']       = 'admin_'.strtolower($set);
  //     $data['password']       = sha1(md5('123456'));
  //     $data['nama_lengkap']   = $key->nama_ketua_rt;
  //     $data['create_time']    = date('Y-m-d H:i:s');
  //     $data['create_by']      = $this->session->userdata('sesi_nama_lengkap');
  //     $data['status_aktif']   = 'Y';
  //     $data['id_user_group']  = 7;
  //     $data['id_wilayah']     = $key->id;
  //     $xss_data = $this->security->xss_clean($data);
  //     $this->db->insert('ref_users', $xss_data);
  //   }
  // }

}

?>
