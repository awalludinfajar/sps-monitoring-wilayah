<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class Data_provinsi extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->isLogin();
    $this->load->library('encrypt');
    $this->load->library('form_validation');
    $this->load->model('m_data_provinsi');
    // $this->load->model('m_wilayah');
  }

  public function isLogin()
  {
    if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('loginapp');
		}
  }

  public function index($value='')
  {
    $data['sub_judul_form']="Data Provinsi";
    $data['breadcrumbs'] = array(
      array (
        'link' => 'welcome',
        'name' => 'Home'
        ),
      array (
        'link' => 'data_provinsi',
        'name' => 'Provinsi'
        )
      );
    $data['role'] = get_role($this->session->userdata('sesi_id'));
    $this->template->load('template_frontend','v_index', $data);
  }

  public function ajax_list($value='')
  {
    $list = $this->m_data_provinsi->get_datatables();

    $data = array();

    $no = $_POST['start'];
    foreach ($list as $datas) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $datas->provinsi;
      $row[] = anchor('data_provinsi/edit/'.$this->encrypt->encode($datas->id), '<i class="fa fa-pencil"></i> Ubah', array('class' => 'btn btn-warning btn-sm'));
      $data[] = $row;
    }
    $output = array(
      'draw' => $_POST['draw'],
      'recordsTotal' => $this->m_data_provinsi->count_all(),
      'recordsFiltered' => $this->m_data_provinsi->count_filtered(),
      'data' => $data
    );

    echo json_encode($output);
  }

  public function add()
  {
    $valid = $this->form_validation;
    $valid->set_rules('provinsi','provinsi','required','xss_clean',array('required'=> '%s Harus Diisi',));

    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Provinsi";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'data_provinsi',
          'name' => 'Provinsi'
        ),
        array (
          'link' => 'data_provinsi/add',
          'name' => 'Tambah Provinsi'
        )
      );
      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_add', $data);
    } else {
      $nama = htmlentities($this->input->post('provinsi'), ENT_QUOTES, 'UTF-8');

      $data['provinsi'] = $nama;
      $this->m_data_provinsi->tambah($data);
      $this->session->set_flashdata('message_sukses','Data Berhasil Ditambah');
      redirect(base_url('data_provinsi/add'),'refresh');
    }
  }

  public function edit($id)
  {
    $kode = $this->encrypt->decode($id);

    $valid = $this->form_validation;
    $valid->set_rules('provinsi','provinsi','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $nama = htmlentities($this->input->post('provinsi'), ENT_QUOTES, 'UTF-8');

    $dataz = $this->m_data_provinsi->detail($kode);
    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Provinsi";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'data_provinsi',
          'name' => 'Provinsi'
        ),
        array (
          'link' => 'data_provinsi/edit/'.$kode,
          'name' => 'Edit Provinsi'
        )
      );
      $data['kode'] = $id;
      $data['isi'] = $dataz;
      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_edit', $data);
    } else {
      $datax['provinsi'] = $nama;

      $this->m_data_provinsi->edit($kode,$datax);
      $this->session->set_flashdata('message_sukses','Data Berhasil Diupdate');
      redirect(base_url('data_provinsi/edit/'.$id),'refresh');
    }
  }

  // link : http://localhost/monitoring_wilayah/data_provinsi/generate
  // public function generate($value='')
  // {
  //   $prv = $this->m_wilayah->allProvinsi();
  //   foreach ($prv as $key) {
  //     $name = explode(" ",$key->provinsi); $set = '';
  //     for ($i=0; $i < count($name); $i++) {
  //       if ($i > count($name)) { $set .= $name[$i].'_'; }
  //       else { $set .= $name[$i]; }
  //     }
  //     $data['username']       = 'admin_'.strtolower($set);
  //     $data['password']       = sha1(md5('123456'));
  //     $data['nama_lengkap']   = $key->provinsi;
  //     $data['create_time']    = date('Y-m-d H:i:s');
  //     $data['create_by']      = $this->session->userdata('sesi_nama_lengkap');
  //     $data['status_aktif']   = 'Y';
  //     $data['id_user_group']  = 2;
  //     $data['id_wilayah']     = $key->id;
  //     $xss_data = $this->security->xss_clean($data);
  //     $this->db->insert('ref_users', $xss_data);
  //   }
  // }

}

?>
