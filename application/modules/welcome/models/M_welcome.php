<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	/**
	 *
	 */
	class M_welcome extends CI_Model
	{

		function __construct()
		{
			parent::__construct();
		}

		public function data_wilayah($poi, $id)
		{
			switch ($poi) {
				case 2:
					return $this->totalprv($id);
					break;
				case 3:
					return $this->totalkab($id);
					break;
				case 4:
					return $this->totalkec($id);
					break;
				case 5:
					return $this->totalkel($id);
					break;
				case 6:
					return $this->totalrrw($id);
					break;
				case 7:
					return $this->totalrrt($id);
					break;
			}
		}

		public function totalprv($id)
		{
			$this->db->group_by('rk.id');
	    $this->db->select('rk.*, COUNT(rdw.id) as jumlah');
	    $this->db->join('ref_data_warga rdw','rk.id = rdw.id_prov', 'left');
	    return $this->db->get_where('ref_provinsi rk', ['rk.id' => $id])->result();
		}

		public function totalkab($id)
		{
			$this->db->group_by('rk.id');
	    $this->db->select('rk.*, COUNT(rdw.id) as jumlah');
	    $this->db->join('ref_data_warga rdw','rk.id = rdw.id_kab', 'left');
	    return $this->db->get_where('ref_kabupaten rk', ['rk.id' => $id])->result();
		}

		public function totalkec($id)
		{
			$this->db->group_by('rk.id');
	    $this->db->select('rk.*, COUNT(rdw.id) as jumlah');
	    $this->db->join('ref_data_warga rdw','rk.id = rdw.id_kec', 'left');
	    return $this->db->get_where('ref_kecamatan rk', ['rk.id' => $id])->result();
		}

		public function totalkel($id)
		{
			$this->db->group_by('rk.id');
	    $this->db->select('rk.*, COUNT(rdw.id) as jumlah');
	    $this->db->join('ref_data_warga rdw','rk.id = rdw.id_kel', 'left');
	    return $this->db->get_where('ref_kelurahan rk', ['rk.id' => $id])->result();
		}

		public function totalrrw($id)
		{
			$this->db->group_by('rk.id');
	    $this->db->select('rk.*, COUNT(rdw.id) as jumlah');
	    $this->db->join('ref_data_warga rdw','rk.id = rdw.id_rw', 'left');
	    return $this->db->get_where('ref_rukun_warga rk', ['rk.id' => $id])->result();
		}

		public function totalrrt($id)
		{
			$this->db->group_by('rk.id');
	    $this->db->select('rk.*, COUNT(rdw.id) as jumlah');
	    $this->db->join('ref_data_warga rdw','rk.id = rdw.id_rt', 'left');
	    return $this->db->get_where('ref_rukun_tetangga rk', ['rk.id' => $id])->result();
		}

	}
?>
