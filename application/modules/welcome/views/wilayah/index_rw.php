<div class="page-header">
	<div class="page-header-content" style="padding:0;">
		<div class="page-title" style="padding-top:0; padding-bottom:15px;">
			<h4>
				<i class="icon-arrow-left52 position-left" onclick="history.go(-1)"></i>
				<span class="text-semibold">Home</span> - Dashboard
				<small class="display-block">Selamat datang, <?php echo $this->session->userdata('sesi_nama_lengkap'); ?></small>
			</h4>
		</div>
	</div>
</div>

<div class="panel panel-flat">
  <div class = "panel-heading" style="padding-bottom:0;">
		<table class="table table-bordered table-striped table-togglable table-hover">
      <tr>
        <th>Wilayah</th>
        <td><?php echo $isi[0]->no_rw.' / '.$isi[0]->nama_ketua_rw; ?></td>
      </tr>
      <tr>
        <th>Jumlah Terdata</th>
        <td><?php echo $isi[0]->jumlah; ?></td>
      </tr>
    </table>
  </div>
  <div class="form-horizontal form-bordered">
    <div class="panel-heading">
      <h1>Data Per Rukun Tetangga (RT)</h1>
      <table id="setta" class="table table-bordered table-striped table-togglable table-hover">
        <thead>
          <tr>
            <th>No</th>
            <th>No.RT / Nama Ketua RT</th>
            <th>No.Hp</th>
            <th>Jumlah</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $n=1; foreach ($result as $key): ?>
            <tr>
              <td><?php echo $n; ?></td>
              <td><?php echo $key->no_rt.' / '.$key->nama_ketua_rt; ?></td>
              <td><?php echo $key->no_hp; ?></td>
              <td><?php echo $key->jumlah; ?></td>
              <td><a class="btn btn-info btn-labeled btn-xs" data-id_nya="<?php echo $key->id; ?>" id="detailwrg"> Detail</a></td>
            </tr>
          <?php $n++; endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
	<div class="form-horizontal form-bordered" id="condition5">
    <div class="panel-heading">
      <h1>Data Warga Per RT</h1>
			<table id="tab_wrg" class="table table-bordered table-striped table-togglable table-hover">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
						<th>Nik</th>
            <th>Foto KTP</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
	$('#condition5').hide();

	$(document).on('click', '#detailwrg', function (e) {
		e.preventDefault();
		let id_kab = $(this).data('id_nya');
		$.ajax({
			type: "get",
			url: "<?= base_url('welcome/wargadata') ?>",
			data: {
				id: id_kab,
			},
			beforeSend: function(){
				$('#condition5').hide();
    	},
			success: function functionName(response) {
				$('#condition5').show();

				$('html, body').animate({
	        scrollTop: eval($("#condition5").offset().top - 70)
	      }, 100);

				$("#tab_wrg > tbody").html(response);
			}
		});
	});
</script>
