<div class="page-header">
	<div class="page-header-content" style="padding:0;">
		<div class="page-title" style="padding-top:0; padding-bottom:15px;">
			<h4>
				<i class="icon-arrow-left52 position-left" onclick="history.go(-1)"></i>
				<span class="text-semibold">Home</span> - Dashboard
				<small class="display-block">Selamat datang, <?php echo $this->session->userdata('sesi_nama_lengkap'); ?></small>
			</h4>
		</div>
	</div>
</div>

<div class="panel panel-flat">
  <div class = "panel-heading" style="padding-bottom:0;">
    <table class="table table-bordered table-striped table-togglable table-hover">
      <tr>
        <th>Wilayah</th>
        <td><?php echo $isi[0]->no_rt.' / '.$isi[0]->nama_ketua_rt; ?></td>
      </tr>
      <tr>
        <th>Jumlah Terdata</th>
        <td><?php echo $isi[0]->jumlah; ?></td>
      </tr>
    </table>
  </div>
  <div class="form-horizontal form-bordered">
    <div class="panel-heading">
    </div>
  </div>
</div>
