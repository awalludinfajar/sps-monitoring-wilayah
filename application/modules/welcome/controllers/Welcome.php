<?php
class Welcome extends MX_Controller{
  //put your code here
  public function __construct() {
    parent::__construct();
		$this->IsLogin();
    $this->load->model('m_welcome');
    $this->load->model('m_wilayah');
  }

	public function IsLogin(){
		if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('loginapp');
		}
  }

	public function index() {
    $data["xxx"]="";
    $userCaptcha = $this->input->post('userCaptcha');

    if ($this->session->userdata('sesi_id_wilayah') != 0) {
      $data['isi']  = $this->m_welcome->data_wilayah($this->session->userdata('sesi_user_group'),$this->session->userdata('sesi_id_wilayah'));
      // var_dump($data['isi']); exit();
    }

    $set = (int)$this->session->userdata('sesi_id_wilayah');
    if ($this->session->userdata('sesi_user_group') == 2) {
      $data['result'] = $this->m_wilayah->allKabupatenJml($set);

    } else if ($this->session->userdata('sesi_user_group') == 3) {
      $data['result'] = $this->m_wilayah->allKecamatanJml($set);

    } else if ($this->session->userdata('sesi_user_group') == 4) {
      $data['result'] = $this->m_wilayah->allKelurahanJml($set);

    } else if ($this->session->userdata('sesi_user_group') == 5) {
      $data['result'] = $this->m_wilayah->allRwJml($set);

    } else if ($this->session->userdata('sesi_user_group') == 6) {
      $data['result'] = $this->m_wilayah->allRtJml($set);

    } else if ($this->session->userdata('sesi_user_group') == 7) {
      $data['result'] = $this->m_wilayah->allWarga($set);
    }

    $set = array('provinsi','kabupaten','kecamatan','kelurahan','rw','rt');
    $kondisi = (int)$this->session->userdata('sesi_user_group')-2;
    $nama = ($kondisi == -1) ? 'v_index' : 'wilayah/index_'.$set[$kondisi];
    // var_dump($this->session->userdata('sesi_user_group'),$nama);
    $this->template->load('template_frontend',$nama, $data);
  }

  public function get_wilayah()
  {
    $id       = $this->input->get('id');
    $wilayah  = $this->input->get('wly');

    $data_kec = $this->m_wilayah->allKecamatan($id);
    $isi = [];
    switch ($wilayah) {
      case 'kab':
        $isi = $this->m_wilayah->allKecamatanJml($id);
        break;
      case 'kec':
        $isi = $this->m_wilayah->allKelurahanJml($id);
        break;
      case 'kel':
        $isi = $this->m_wilayah->allRwJml($id);
        break;
      case 'rrw':
        $isi = $this->m_wilayah->allRtJml($id);
        break;
    }

    $i = 1; $row = '';
    foreach ($isi as $item) {
      $row .= '<tr>';
			$row .= '<td>'.$i++.'</td>';
      $text = '';
      if ($wilayah == 'kab') {
        $row .= '<td>'.$item->kecamatan.'</td>';
        $text = 'detailkel';
      } else if ($wilayah == 'kec') {
        $row .= '<td>'.$item->kelurahan.'</td>';
        $text = 'detailrrw';
      } else if ($wilayah == 'kel') {
        $row .= '<td> RW'.$item->no_rw.' / '.$item->nama_ketua_rw.'</td>';
        $text = 'detailrrt';
      } else if ($wilayah == 'rrw') {
        $row .= '<td> RT'.$item->no_rt.' / '.$item->nama_ketua_rt.'</td>';
        $text = 'detailwrg';
      }
      $row .= '<td>'.$item->jumlah.'</td>';
      $row .= '<td>'.anchor('#', 'Detail', array('class' => 'btn btn-info btn-xs', 'data-id_nya' => $item->id, 'id' => $text)).'</td>';
      $row .= '</tr>';
    }
    echo $row;
  }

  public function wargadata()
  {
    $id = $this->input->get('id');
    $has = $this->m_wilayah->allWarga($id);
    $i = 1; $row = '';
    foreach ($has as $key) {
      $row .= '<tr>';
      $row .= '<td>'.$i++.'</td>';
      $row .= '<td>'.$key->nama_warga.'</td>';
      $row .= '<td>'.$key->nik_warga.'</td>';
      $row .= '<td><img src="'.base_url().'uploads/foto_ktp/'.$key->img_ktp.'" width="90" height="60"></td>';
      $row .= '<tr>';
    }
    echo $row;
  }

}
?>
