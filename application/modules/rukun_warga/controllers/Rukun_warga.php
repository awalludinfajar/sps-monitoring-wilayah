<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class Rukun_warga extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->isLogin();
    $this->load->library('encrypt');
    $this->load->library('form_validation');
    $this->load->model('m_rukun_warga');
    $this->load->model('m_wilayah');
  }

  public function isLogin()
  {
    if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('loginapp');
		}
  }

  public function index($value='')
  {
    $data['sub_judul_form']="Data Rukun Warga (Rw)";
    $data['breadcrumbs'] = array(
      array (
        'link' => 'welcome',
        'name' => 'Home'
        ),
      array (
        'link' => 'rukun_warga',
        'name' => 'Rukun Warga'
        )
      );
    $data['role'] = get_role($this->session->userdata('sesi_id'));
    $this->template->load('template_frontend','v_index', $data);
  }

  public function ajax_list($value='')
  {
    $list = $this->m_rukun_warga->get_datatables();

    $data = array();

    $no = $_POST['start'];
    foreach ($list as $datas) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $datas->kelurahan.'('.$datas->kd_pos.')';
      $row[] = $datas->no_rw;
      $row[] = $datas->nama_ketua_rw;
      $row[] = anchor('rukun_warga/edit/'.$this->encrypt->encode($datas->id), '<i class="fa fa-pencil"></i> Ubah', array('class' => 'btn btn-warning btn-sm'));
      $data[] = $row;
    }
    $output = array(
      'draw' => $_POST['draw'],
      'recordsTotal' => $this->m_rukun_warga->count_all(),
      'recordsFiltered' => $this->m_rukun_warga->count_filtered(),
      'data' => $data
    );

    echo json_encode($output);
  }

  public function add($value='')
  {
    $valid = $this->form_validation;
    $valid->set_rules('kelurahan_id','kelurahan_id','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('no_rw','no_rw','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('n_ketua_rw','n_ketua_rw','required','xss_clean',array('required'=> '%s Harus Diisi',));

    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Rukun Warga (RW)";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'rukun_warga',
          'name' => 'Rukun Warga'
        ),
        array (
          'link' => 'rukun_warga/add',
          'name' => 'Tambah RW'
        )
      );

      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_add', $data);
    } else {
      $kelurahan  = htmlentities($this->input->post('kelurahan_id'), ENT_QUOTES, 'UTF-8');
      $no_rw      = htmlentities($this->input->post('no_rw'), ENT_QUOTES, 'UTF-8');
      $n_ketua_rw = htmlentities($this->input->post('n_ketua_rw'), ENT_QUOTES, 'UTF-8');

      $data['kelurahan_id'] = $kelurahan;
      $data['no_rw'] = $no_rw;
      $data['nama_ketua_rw'] = $n_ketua_rw;
      $this->m_rukun_warga->tambah($data);
      $this->session->set_flashdata('message_sukses','Data Berhasil Ditambah');
      redirect(base_url('rukun_warga/add'),'refresh');
    }
  }

  public function edit($id)
  {
    $kode = $this->encrypt->decode($id);

    $valid = $this->form_validation;
    $valid->set_rules('kelurahan_id','kelurahan_id','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('no_rw','no_rw','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('n_ketua_rw','n_ketua_rw','required','xss_clean',array('required'=> '%s Harus Diisi',));

    $kelurahan  = htmlentities($this->input->post('kelurahan_id'), ENT_QUOTES, 'UTF-8');
    $no_rw      = htmlentities($this->input->post('no_rw'), ENT_QUOTES, 'UTF-8');
    $n_ketua_rw = htmlentities($this->input->post('n_ketua_rw'), ENT_QUOTES, 'UTF-8');

    $dataz = $this->m_rukun_warga->detail($kode);
    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Rukun Warga (RW)";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'rukun_warga',
          'name' => 'Rukun Warga'
        ),
        array (
          'link' => 'rukun_warga/edit/'.$id,
          'name' => 'Edit RW'
        )
      );

      $data['kode'] = $id;
      $data['isi'] = $dataz;
      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_edit', $data);
    } else {
      $datax['kelurahan_id'] = $kelurahan;
      $datax['no_rw'] = $no_rw;
      $datax['nama_ketua_rw'] = $n_ketua_rw;

      $this->m_rukun_warga->edit($kode,$datax);
      $this->session->set_flashdata('message_sukses','Data Berhasil Diupdate');
      redirect(base_url('rukun_warga/edit/'.$id),'refresh');
    }

  }

  // link : http://localhost/monitoring_wilayah/rukun_warga/generate
  // public function generate($value='')
  // {
  //   $prv = $this->m_wilayah->allRw();
  //   foreach ($prv as $key) {
  //     $name = explode(" ",$key->nama_ketua_rw); $set = '';
  //     for ($i=0; $i < count($name); $i++) {
  //       if ($i > count($name)) { $set .= $name[$i].'_'; }
  //       else { $set .= $name[$i]; }
  //     }
  //     $data['username']       = 'admin_'.strtolower($set);
  //     $data['password']       = sha1(md5('123456'));
  //     $data['nama_lengkap']   = $key->nama_ketua_rw;
  //     $data['create_time']    = date('Y-m-d H:i:s');
  //     $data['create_by']      = $this->session->userdata('sesi_nama_lengkap');
  //     $data['status_aktif']   = 'Y';
  //     $data['id_user_group']  = 6;
  //     $data['id_wilayah']     = $key->id;
  //     $xss_data = $this->security->xss_clean($data);
  //     $this->db->insert('ref_users', $xss_data);
  //   }
  // }

}

?>
