<div class="page-header">
	<div class="page-header-content" style="padding:0;">
		<div class="page-title" style="padding-top:0; padding-bottom:15px;">
			<h4>
				<i class="icon-arrow-left52 position-left"></i>
				<span class="text-semibold"><?php echo $sub_judul_form;?></span>
			</h4>
			<ul class="breadcrumb breadcrumb-caret position-right">
				<?php foreach ($breadcrumbs as $key => $value) { ?>
				<li>
					<a href=<?php echo site_url($value['link'])?> > <?php echo $value['name']; ?></a>
					<?php echo (count($breadcrumbs)-1)==$key?"":""; ?>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>

<div class="panel panel-flat">
  <div class="panel-heading">
    <legend class="text-semibold">Tambah Data</legend>
    <div class="panel-body" style="padding:0;">
      <?php if ($this->session->flashdata('message_gagal')) {
        echo '<div class="alert alert-warning"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
      }
      if ($this->session->flashdata('message_sukses')) {
        echo '<div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
      } ?>
			<?php echo form_open('data_warga/send_add',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>
      <div class="form-group" id="view0">
        <label class="col-lg-1 control-label">Upload KTP</label>
        <div class="form-group">
          <div class="col-lg-4">
            <img id="bordimage0" src="<?php echo base_url(); ?>/assets/img/attachment.jpg" alt="" style="border: 1px solid #adadad; width: 410px; height: 200px;">
          </div>
					<div class="col-lg-3">
						1
            <input type="file" name="ktp0" class="form-control" value="" id="setImage0">&nbsp;
						<input placeholder="Nip" type="text" name="nikwg0" class="form-control" value="">&nbsp;
            <input placeholder="Nama" type="text" name="nama_warga0" id="nama_warga0" class="form-control" value="">
            <p>&nbsp;</p>
            <!-- <a class="btn btn-info btn-xs" id="add_dta0"><i class="icon-file-plus"></i></a> -->
          </div>
        </div>
      </div>

      <?php for ($i=1; $i < 10; $i++) { ?>
      <div class="form-group" id="view<?php echo $i; ?>">
        <label class="col-lg-1 control-label"></label>
        <div class="form-group">
          <div class="col-lg-4">
						<img id="bordimage<?php echo $i; ?>" src="<?php echo base_url(); ?>/assets/img/attachment.jpg" alt="" style="border: 1px solid #adadad; width: 410px; height: 200px;">
          </div>
          <div class="col-lg-3">
						<?php echo $i+1; ?>
            <input type="file" name="ktp<?php echo $i; ?>" class="form-control" value="" id="setImage<?php echo $i; ?>">&nbsp;
						<input placeholder="Nip" type="text" name="nikwg<?php echo $i; ?>" class="form-control" value="">&nbsp;
            <input placeholder="Nama" type="text" name="nama_warga<?php echo $i; ?>" id="nama_warga<?php echo $i; ?>" class="form-control" value="">
            <p>&nbsp;</p>
            <!-- <php if ($i != 9): ?> -->
              <!-- <a class="btn btn-info btn-xs" id="add_dta<?php echo $i; ?>"><i class="icon-file-plus"></i></a> -->
            <!-- <php endif; ?> -->
            <!-- <a class="btn btn-danger btn-xs" id="rmv_dta<?php echo $i; ?>"><i class="icon-trash"></i></a> -->
          </div>
        </div>
      </div>
      <?php } ?>

      <div class="text-right col-lg-8">
        <button type="submit" class="btn btn-success btn-labeled btn-xs"><b><i class="icon-files-empty2"></i></b> Simpan</button>
        <a class="btn btn-danger btn-labeled btn-xs"  href="<?php echo site_url();?>data_warga"><b><i class="icon-arrow-left13"></i></b> Kembali</a>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  // hide ---------------------------------------------------
  // $('#view1').hide();
  // $('#view2').hide();
  // $('#view3').hide();
  // $('#view4').hide();
  // $('#view5').hide();
  // $('#view6').hide();
  // $('#view7').hide();
  // $('#view8').hide();
  // $('#view9').hide();

  // add ---------------------------------------------------
  // $('#add_dta0').click(function () { $('#view1').show(); });
  // $('#add_dta1').click(function () { $('#view2').show(); });
  // $('#add_dta2').click(function () { $('#view3').show(); });
  // $('#add_dta3').click(function () { $('#view4').show(); });
  // $('#add_dta4').click(function () { $('#view5').show(); });
  // $('#add_dta5').click(function () { $('#view6').show(); });
  // $('#add_dta6').click(function () { $('#view7').show(); });
  // $('#add_dta7').click(function () { $('#view8').show(); });
  // $('#add_dta8').click(function () { $('#view9').show(); });

  // remove ---------------------------------------------------
  // $('#rmv_dta1').click(function () { $('#view1').hide(); $('#setImage1').val(''); $('#nama_warga1').val(''); $('#bordimage1').attr('src', ''); });
  // $('#rmv_dta2').click(function () { $('#view2').hide(); $('#setImage2').val(''); $('#nama_warga2').val(''); $('#bordimage2').attr('src', ''); });
  // $('#rmv_dta3').click(function () { $('#view3').hide(); $('#setImage3').val(''); $('#nama_warga3').val(''); $('#bordimage3').attr('src', ''); });
  // $('#rmv_dta4').click(function () { $('#view4').hide(); $('#setImage4').val(''); $('#nama_warga4').val(''); $('#bordimage4').attr('src', ''); });
  // $('#rmv_dta5').click(function () { $('#view5').hide(); $('#setImage5').val(''); $('#nama_warga5').val(''); $('#bordimage5').attr('src', ''); });
  // $('#rmv_dta6').click(function () { $('#view6').hide(); $('#setImage6').val(''); $('#nama_warga6').val(''); $('#bordimage6').attr('src', ''); });
  // $('#rmv_dta7').click(function () { $('#view7').hide(); $('#setImage7').val(''); $('#nama_warga7').val(''); $('#bordimage7').attr('src', ''); });
  // $('#rmv_dta8').click(function () { $('#view8').hide(); $('#setImage8').val(''); $('#nama_warga8').val(''); $('#bordimage8').attr('src', ''); });
  // $('#rmv_dta9').click(function () { $('#view9').hide(); $('#setImage9').val(''); $('#nama_warga9').val(''); $('#bordimage9').attr('src', ''); });

  // setImage Per No ----------------------------------------

	// $(document).ready(function() {
	// });
	$('input[placeholder=Nama]').change(function () {

		console.log($(this).val());
	});

  $('#setImage0').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#bordimage0').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  $('#setImage1').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#bordimage1').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  $('#setImage2').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#bordimage2').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  $('#setImage3').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#bordimage3').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  $('#setImage4').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#bordimage4').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  $('#setImage5').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#bordimage5').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  $('#setImage6').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#bordimage6').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  $('#setImage7').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#bordimage7').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  $('#setImage8').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#bordimage8').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  $('#setImage9').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#bordimage9').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });
</script>
