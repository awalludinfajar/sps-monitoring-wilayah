<div class="page-header">
	<div class="page-header-content" style="padding:0;">
		<div class="page-title" style="padding-top:0; padding-bottom:15px;">
			<h4>
				<i class="icon-arrow-left52 position-left"></i>
				<span class="text-semibold"><?php echo $sub_judul_form;?></span>
			</h4>
			<ul class="breadcrumb breadcrumb-caret position-right">
				<?php foreach ($breadcrumbs as $key => $value) { ?>
				<li>
					<a href=<?php echo site_url($value['link'])?> > <?php echo $value['name']; ?></a>
					<?php echo (count($breadcrumbs)-1)==$key?"":""; ?>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>

<div class="panel panel-flat">
  <div class = "panel-heading" style="padding-bottom:0;">
    <h5 class="panel-title"><?php echo $sub_judul_form;?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
    <div class="heading-elements">
  		<ul class="icons-list" style="margin-top:10px;">
  			<li><a style="color:#fff;"class="btn btn-success btn-labeled btn-xs" href="<?php echo ($this->session->userdata('sesi_user_group') == 1) ? site_url("data_warga/add") : site_url("data_warga/auto_add") ; ?>"><b><i class="icon-plus3"></i></b> Tambah</a></li>
  		</ul>
  	</div>
  </div>
  <hr style="margin-top:10px;margin-bottom:5px;">
  <form class="form-horizontal form-bordered">
    <div class="panel-heading">
      <div class="table-responsive">
        <table id="myTable" class="table table-bordered table-striped table-togglable table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Alamat</th>
              <th>Nama</th>
							<th>Nik</th>
              <th>Foto KTP</th>
              <!-- <php if ($role['update'] == "TRUE"): ?> -->
                <!-- <th>Aksi</th> -->
              <!-- <php endif; ?> -->
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    show_provinsi();
    function show_provinsi(){
     table = $('#myTable').DataTable({
       "processing": true,
       "serverSide": true,
       "bDestroy": true,
       "order": [],
       "language": {
         "processing": '<i class="fa fa-refresh fa-spin fa-3x fa-fw" style="color:#fff"></i><span>Mohon Tunggu...</span>'
     },

     "ajax": {
       "url": "<?php echo base_url('data_warga/ajax_list')?>",
       "type": "POST"
     },
     "columnDefs": [
         {
           "targets": [ -1 ],
           "orderable": false,
         },
       ],
     });
    };
  });
</script>
