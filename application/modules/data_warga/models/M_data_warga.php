<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class m_data_warga extends CI_Model
{

  var $tbl = 'ref_data_warga';
  var $column_search = array('rp.provinsi','rka.kabupaten_kota','rka.ibukota','rke.kecamatan','rkl.kelurahan','rkl.kd_pos','rrw.no_rw','rrw.nama_ketua_rw','rrt.no_rt','rrt.nama_ketua_rt','rrt.no_hp','rdw.nama_warga','rdw.nik_warga','rdw.img_ktp');
  var $column_order = array(null,'nama_warga');
  var $order = array('id'=> 'ASC');

  function __construct()
  {
    parent::__construct();
  }

  public function get_datatables($value='')
  {
    $this->_get_datatables_query($value);
    if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  public function _get_datatables_query($value='')
  {
    $this->db->select('rdw.id, rp.provinsi, rka.kabupaten_kota, rka.ibukota, rke.kecamatan, rkl.kelurahan, rkl.kd_pos, rrw.no_rw, rrw.nama_ketua_rw, rrt.no_rt, rrt.nama_ketua_rt, rrt.no_hp, rdw.nama_warga, rdw.nik_warga, rdw.img_ktp');
    $this->db->join('ref_rukun_tetangga rrt','rrt.id=rdw.id_rt');
    $this->db->join('ref_rukun_warga rrw','rrw.id=rdw.id_rw');
    $this->db->join('ref_kelurahan rkl','rkl.id=rdw.id_kel');
    $this->db->join('ref_kecamatan rke','rke.id=rdw.id_kec');
    $this->db->join('ref_kabupaten rka','rka.id=rdw.id_kab');
    $this->db->join('ref_provinsi rp','rp.id=rdw.id_prov');
    $this->db->from('ref_data_warga rdw');
    if ($value != '') {
      $this->db->where($value);
    }

    $i = 0;

    foreach ($this->column_search as $item) {

      if($_POST['search']['value']) {
        if($i===0) {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order'])) { // here order processing
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if(isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->select('rdw.id, rp.provinsi, rka.kabupaten_kota, rka.ibukota, rke.kecamatan, rkl.kelurahan, rkl.kd_pos, rrw.no_rw, rrw.nama_ketua_rw, rrt.no_rt, rrt.nama_ketua_rt, rrt.no_hp, rdw.nama_warga, rdw.img_ktp');
    $this->db->join('ref_rukun_tetangga rrt','rrt.id=rdw.id_rt');
    $this->db->join('ref_rukun_warga rrw','rrw.id=rdw.id_rw');
    $this->db->join('ref_kelurahan rkl','rkl.id=rdw.id_kel');
    $this->db->join('ref_kecamatan rke','rke.id=rdw.id_kec');
    $this->db->join('ref_kabupaten rka','rka.id=rdw.id_kab');
    $this->db->join('ref_provinsi rp','rp.id=rdw.id_prov');
    $this->db->from('ref_data_warga rdw');
    return $this->db->count_all_results();
  }

  public function tambah($data)
  {
    $this->db->insert($this->tbl,$data);
  }
}

?>
