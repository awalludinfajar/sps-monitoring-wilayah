<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class Data_warga extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->isLogin();
    $this->load->library('encrypt');
    $this->load->library('form_validation');
    $this->load->model('m_data_warga');
    $this->load->model('rukun_tetangga/m_rukun_tetangga');
    $this->load->model('data_kelurahan/m_kelurahan');
    $this->load->model('data_kecamatan/m_kecamatan');
    $this->load->model('data_kota_kabupaten/m_kota_kabupaten');
  }

  public function isLogin()
  {
    if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('loginapp');
		}
  }

  public function index($value='')
  {
    $data['sub_judul_form']="Data Warga";
    $data['breadcrumbs'] = array(
      array (
        'link' => 'welcome',
        'name' => 'Home'
        ),
      array (
        'link' => 'data_warga',
        'name' => 'Data Warga'
        )
      );
    $data['role'] = get_role($this->session->userdata('sesi_id'));
    $this->template->load('template_frontend','v_index', $data);
  }

  public function ajax_list($value='')
  {
    $cekid = $this->session->userdata('sesi_user_group');
    $wilya = $this->session->userdata('sesi_id_wilayah');
    if ($cekid != 1) {
      if ($cekid == 2) {
        $set = ['rdw.id_prov' => $wilya];
      } else if ($cekid == 3) {
        $set = ['rdw.id_kab' => $wilya];
      } else if ($cekid == 4) {
        $set = ['rdw.id_kec' => $wilya];
      } else if ($cekid == 5) {
        $set = ['rdw.id_kel' => $wilya];
      } else if ($cekid == 6) {
        $set = ['rdw.id_rw' => $wilya];
      } else if ($cekid == 7) {
        $set = ['rdw.id_rt' => $wilya];
      }
    } else {
      $set = '';
    }

    $list = $this->m_data_warga->get_datatables($set);

    $data = array();

    $no = $_POST['start'];
    foreach ($list as $datas) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = 'RT '.$datas->no_rt.' / RW '.$datas->no_rw.' '.$datas->kelurahan.' '.$datas->kecamatan.' '.$datas->kabupaten_kota.'('.$datas->ibukota.')'.' '.$datas->provinsi;
      $row[] = $datas->nama_warga;
      $row[] = $datas->nik_warga;
      $row[] = '<img src="'.base_url().'uploads/foto_ktp/'.$datas->img_ktp.'" width="80" height="50">';
      // $row[] = anchor('data_warga/edit/'.$this->encrypt->encode($datas->id), '<i class="fa fa-pencil"></i> Ubah', array('class' => 'btn btn-warning btn-sm'));
      $data[] = $row;
    }
    $output = array(
      'draw' => $_POST['draw'],
      'recordsTotal' => $this->m_data_warga->count_all(),
      'recordsFiltered' => $this->m_data_warga->count_filtered(),
      'data' => $data
    );

    echo json_encode($output);
  }

  function upload_config_iden($imgpath, $filetype, $cek)
	{
		$config['upload_path'] 		= $imgpath;
		$config['allowed_types'] 	= $filetype;
		$config['encrypt_name'] 	= TRUE;
		$config['file_name'] 		= 'ktp-'.time().'-'.$cek;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
	}

  public function add($value='')
  {
    $valid = $this->form_validation;
    $valid->set_rules('nama_provinsi','nama_provinsi','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('nama_kotakab','nama_kotakab','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('kecamatan_id','kecamatan_id','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('kelurahan_id','kelurahan_id','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('ketua_rw_id','ketua_rw_id','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('ketua_rt_id','ketua_rt_id','required','xss_clean',array('required'=> '%s Harus Diisi',));

    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Warga";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'data_warga',
          'name' => 'Data Warga'
        ),
        array (
          'link' => 'data_warga/add',
          'name' => 'Tambah Warga'
        )
      );

      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_add', $data);
    } else {

      $id_prv  = htmlentities($this->input->post('nama_provinsi'), ENT_QUOTES, 'UTF-8');
      $id_kab  = htmlentities($this->input->post('nama_kotakab'), ENT_QUOTES, 'UTF-8');
      $id_kec  = htmlentities($this->input->post('kecamatan_id'), ENT_QUOTES, 'UTF-8');
      $id_kel  = htmlentities($this->input->post('kelurahan_id'), ENT_QUOTES, 'UTF-8');
      $id_rrw  = htmlentities($this->input->post('ketua_rw_id'), ENT_QUOTES, 'UTF-8');
      $id_rrt  = htmlentities($this->input->post('ketua_rt_id'), ENT_QUOTES, 'UTF-8');

      $tampung_notif = array();
      for ($i=0; $i < 10; $i++) {
        $valid->set_rules('nama_warga','nama_warga','required','xss_clean',array('required'=> '%s Harus Diisi',));
        $valid->set_rules('nikwg','nikwg','required','xss_clean',array('required'=> '%s Harus Diisi',));

        $nama_warga  = htmlentities($this->input->post('nama_warga'.$i), ENT_QUOTES, 'UTF-8');
        $nik_warga   = htmlentities($this->input->post('nikwg'.$i), ENT_QUOTES, 'UTF-8');

        $att_ktp = 'ktp'.$i;
        if (!empty($_FILES[$att_ktp]["tmp_name"])) {
          $this->upload_config_iden('./uploads/foto_ktp/','jpg|jpeg|png');
          $foto_ktp_upload = $this->upload->do_upload($att_ktp);
          $data_foto_ktp = $this->upload->data();

          $tampung_notif[] = ($foto_ktp_upload == TRUE) ? 'true' : 'false';

          if ($nama_warga != '' && $nik_warga != '' && $foto_ktp_upload) {
            $this->thumb_create('./uploads/foto_ktp/'.$data_foto_ktp['file_name'], './uploads/foto_ktp/thmb/');
            $datax['id_prov']    = $id_prv;
            $datax['id_kab']     = $id_kab;
            $datax['id_kec']     = $id_kec;
            $datax['id_kel']     = $id_kel;
            $datax['id_rw']      = $id_rrw;
            $datax['id_rt']      = $id_rrt;
            $datax['nama_warga'] = $nama_warga;
            $datax['nik_warga']  = $nik_warga;
            $datax['img_ktp']    = $data_foto_ktp['file_name'];
            $this->m_data_warga->tambah($datax);
            $this->session->set_flashdata('message_sukses','Data Berhasil Ditambah');
          }
        }
      }

      redirect(base_url('data_warga/add'),'refresh');
    }
  }

  public function auto_add($value='')
  {
    $data['sub_judul_form']="Data Warga";
    $data['breadcrumbs'] = array(
      array (
        'link' => 'welcome',
        'name' => 'Home'
      ),
      array (
        'link' => 'data_warga',
        'name' => 'Data Warga'
      ),
      array (
        'link' => 'data_warga/auto_add',
        'name' => 'Tambah Warga'
      )
    );

    $data['role'] = get_role($this->session->userdata('sesi_id'));
    $this->template->load('template_frontend','v_auto_add', $data);
  }

  public function send_add($value='')
  {
    $get_up = $this->m_rukun_tetangga->detail($this->session->userdata('sesi_id_wilayah'));
    $get_id_kc = $this->m_kelurahan->detail($get_up['kelurahan_id']);
    $get_id_kb = $this->m_kecamatan->detail($get_id_kc['kecamatan_id']);
    $get_id_pv = $this->m_kota_kabupaten->detail($get_id_kb['kabkot_id']);

    $id_prv = $get_id_pv['provinsi_id'];
    $id_kab = $get_id_kb['kabkot_id'];
    $id_kec = $get_id_kc['kecamatan_id'];
    $id_kel = $get_up['kelurahan_id'];
    $id_rrw = $get_up['rw_id'];
    $id_rrt = $this->session->userdata('sesi_id_wilayah');

    $dat = array();
    for ($i=0; $i < 10; $i++) {

      $namwg = htmlentities($this->input->post('nama_warga'.$i), ENT_QUOTES, 'UTF-8');
      $nomkk = htmlentities($this->input->post('nikwg'.$i), ENT_QUOTES, 'UTF-8');

      $att_ktp = 'ktp'.$i;
      if (!empty($_FILES[$att_ktp]["tmp_name"])) {
        $this->upload_config_iden('./uploads/foto_ktp/','jpg|jpeg|png', $nomkk);
        $foto_ktp_upload = $this->upload->do_upload($att_ktp);
        $data_foto_ktp = $this->upload->data();

        if ($foto_ktp_upload && $namwg != '' && $nomkk != '') {
          $this->thumb_create('./uploads/foto_ktp/'.$data_foto_ktp['file_name'], './uploads/foto_ktp/thmb/');
          $datax['id_prov']    = $id_prv;
          $datax['id_kab']     = $id_kab;
          $datax['id_kec']     = $id_kec;
          $datax['id_kel']     = $id_kel;
          $datax['id_rw']      = $id_rrw;
          $datax['id_rt']      = $id_rrt;
          $datax['nama_warga'] = $namwg;
          $datax['nik_warga']  = $nomkk;
          $datax['img_ktp']    = $data_foto_ktp['file_name'];
          $dat[] = $datax;
        }
      }
    }
    if (count($dat) > 0) {
      for ($i=0; $i < count($dat); $i++) {
        $this->m_data_warga->tambah($dat[$i]);
      }
      $this->session->set_flashdata('message_sukses','Data Berhasil Ditambah');
      redirect(base_url('data_warga/auto_add'),'refresh');
    } else {
      $this->session->set_flashdata('message_gagal','Tidak ada data di tambahkan');
      redirect(base_url('data_warga/auto_add'),'refresh');
    }
  }

  public function thumb_create($source, $new_loc)
  {
    $config['image_library']  = 'gd2';
    $config['source_image']   = $source;
    $config['new_image']      = $new_loc;
    $config['maintain_ratio'] = TRUE;
    $config['create_thumb']   = TRUE;
    $config['quality']			  = '50%';
    $config['thumb_marker']   = '_thumb';
    $config['width']          = 115;
    $config['height']         = 75;
    $this->load->library('image_lib', $config);
    $this->image_lib->resize();
    $this->image_lib->clear();
  }
}

?>
