<div class="page-header">
	<div class="page-header-content" style="padding:0;">
		<div class="page-title" style="padding-top:0; padding-bottom:15px;">
			<h4>
				<i class="icon-arrow-left52 position-left"></i>
				<span class="text-semibold"><?php echo $sub_judul_form;?></span>
			</h4>
			<ul class="breadcrumb breadcrumb-caret position-right">
				<?php foreach ($breadcrumbs as $key => $value) { ?>
				<li>
					<a href=<?php echo site_url($value['link'])?> > <?php echo $value['name']; ?></a>
					<?php echo (count($breadcrumbs)-1)==$key?"":""; ?>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>

<div class="panel panel-flat">
  <div class = "panel-heading">
    <legend class="text-semibold">Tambah Kelurahan</legend>
    <div class="panel-body" style="padding:0;">
      <?php echo form_open('data_kelurahan/add',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>
        <?php
        if ($this->session->flashdata('message_gagal')) {
          echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
          echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
        }?>
        <div class="form-group">
    			<label class="col-lg-1 control-label">Nama Provinsi</label>
    			<div class="col-lg-6">
            <select data-rule-required="true" name="nama_provinsi" class="select-search" id="nama_provinsi"></select>
    			</div>
    		</div>
        <div class="form-group">
    			<label class="col-lg-1 control-label">Nama Kota/Kabupaten</label>
    			<div class="col-lg-6">
            <select data-rule-required="true" name="nama_kotakab" class="select-search" id="nama_kotakab"></select>
    			</div>
    		</div>
        <div class="form-group">
    			<label class="col-lg-1 control-label">Nama Kecamatan</label>
    			<div class="col-lg-6">
						<select data-rule-required="true" name="kecamatan_id" class="select-search" id="kecamatan_id"></select>
    			</div>
    		</div>
        <div class="form-group">
    			<label class="col-lg-1 control-label">Nama Kelurahan</label>
    			<div class="col-lg-6">
						<input required placeholder="Kecamatan" type="text" name="kelurahan" class="form-control" data-rule-required="true" value="">
    			</div>
    		</div>
        <div class="form-group">
    			<label class="col-lg-1 control-label">Kode Pos</label>
    			<div class="col-lg-6">
						<input required placeholder="Kode Pos" type="text" name="kode_pos" class="form-control" data-rule-required="true" value="">
    			</div>
    		</div>
        <div class="text-right col-lg-7">
    			<button type="submit" class="btn btn-success btn-labeled btn-xs"><b><i class="icon-files-empty2"></i></b> Simpan</button>
    			<a class="btn btn-danger btn-labeled btn-xs"  href="<?php echo site_url();?>data_kelurahan"><b><i class="icon-arrow-left13"></i></b> Kembali</a>
    		</div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#nama_provinsi').select2();
  $('#nama_kotakab').select2();
  $('#kecamatan_id').select2();
  $(document).ready(function() {
    var url = "<?php echo site_url('api/selectProvinsi'); ?>";
		$('#nama_provinsi').load(url);
  });

  $('#nama_provinsi').change(function() {
    var url = "<?php echo site_url('api/selectKotaKab'); ?>/"+$(this).val();
		$('#nama_kotakab').load(url);
    return false;
  });

  $('#nama_kotakab').change(function() {
    var url = "<?php echo site_url('api/selectKecamatan'); ?>/"+$(this).val();
    $('#kecamatan_id').load(url);
    return false;
  });
</script>
