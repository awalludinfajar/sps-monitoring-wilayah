<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class Data_kelurahan extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->isLogin();
    $this->load->library('encrypt');
    $this->load->library('form_validation');
    $this->load->model('m_kelurahan');
    // $this->load->model('m_wilayah');
  }

  public function isLogin()
  {
    if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('loginapp');
		}
  }

  public function index($value='')
  {
    $data['sub_judul_form']="Data Kelurahan";
    $data['breadcrumbs'] = array(
      array (
        'link' => 'welcome',
        'name' => 'Home'
        ),
      array (
        'link' => 'data_kelurahan',
        'name' => 'Kelurahan'
        )
      );
    $data['role'] = get_role($this->session->userdata('sesi_id'));
    $this->template->load('template_frontend','v_index', $data);
  }

  public function ajax_list($value='')
  {
    $list = $this->m_kelurahan->get_datatables();

    $data = array();

    $no = $_POST['start'];
    foreach ($list as $datas) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $datas->kabupaten_kota.' - '.$datas->ibukota;
      $row[] = $datas->kecamatan;
      $row[] = $datas->kelurahan.'('.$datas->kd_pos.')';
      $row[] = anchor('data_kelurahan/edit/'.$this->encrypt->encode($datas->id), '<i class="fa fa-pencil"></i> Ubah', array('class' => 'btn btn-warning btn-sm'));
      $data[] = $row;
    }
    $output = array(
      'draw' => $_POST['draw'],
      'recordsTotal' => $this->m_kelurahan->count_all(),
      'recordsFiltered' => $this->m_kelurahan->count_filtered(),
      'data' => $data
    );

    echo json_encode($output);
  }

  public function add($value='')
  {
    $valid = $this->form_validation;
    $valid->set_rules('kecamatan_id','kecamatan_id','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('kelurahan','kelurahan','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('kode_pos','kode_pos','required','xss_clean',array('required'=> '%s Harus Diisi',));

    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Kelurahan";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'data_kelurahan',
          'name' => 'Kecamatan'
        ),
        array (
          'link' => 'data_kelurahan/add',
          'name' => 'Tambah Kecamatan'
        )
      );
      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_add', $data);
    } else {
      $id_kecamatan  = htmlentities($this->input->post('kecamatan_id'), ENT_QUOTES, 'UTF-8');
      $kelurahan  = htmlentities($this->input->post('kelurahan'), ENT_QUOTES, 'UTF-8');
      $kode_pos  = htmlentities($this->input->post('kode_pos'), ENT_QUOTES, 'UTF-8');

      $data['kecamatan_id'] = $id_kecamatan;
      $data['kelurahan'] = $kelurahan;
      $data['kd_pos'] = $kode_pos;
      $this->m_kelurahan->tambah($data);
      $this->session->set_flashdata('message_sukses','Data Berhasil Ditambah');
      redirect(base_url('data_kelurahan/add'),'refresh');
    }
  }

  public function edit($id)
  {
    $kode = $this->encrypt->decode($id);

    $valid = $this->form_validation;
    $valid->set_rules('kecamatan_id','kecamatan_id','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('kelurahan','kelurahan','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('kode_pos','kode_pos','required','xss_clean',array('required'=> '%s Harus Diisi',));

    $id_kecamatan  = htmlentities($this->input->post('kecamatan_id'), ENT_QUOTES, 'UTF-8');
    $kelurahan  = htmlentities($this->input->post('kelurahan'), ENT_QUOTES, 'UTF-8');
    $kode_pos  = htmlentities($this->input->post('kode_pos'), ENT_QUOTES, 'UTF-8');

    $dataz = $this->m_kelurahan->detail($kode);
    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Kelurahan";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'data_kelurahan',
          'name' => 'Kecamatan'
        ),
        array (
          'link' => 'data_kelurahan/edit/'.$id,
          'name' => 'Edit Kelurahan'
        )
      );
      $data['kode'] = $id;
      $data['isi'] = $dataz;
      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_edit', $data);
    } else {
      $datax['kecamatan_id'] = $id_kecamatan;
      $datax['kelurahan']    = $kelurahan;
      $datax['kd_pos']       = $kode_pos;

      $this->m_kelurahan->edit($kode,$datax);
      $this->session->set_flashdata('message_sukses','Data Berhasil Diupdate');
      redirect(base_url('data_kelurahan/edit/'.$id),'refresh');
    }

  }

  // link : http://localhost/monitoring_wilayah/data_kelurahan/generate
  // public function generate($value='')
  // {
  //   $prv = $this->m_wilayah->allKelurahan();
  //   foreach ($prv as $key) {
  //     $name = explode(" ",$key->kelurahan); $set = '';
  //     for ($i=0; $i < count($name); $i++) {
  //       if ($i > count($name)) { $set .= $name[$i].'_'; }
  //       else { $set .= $name[$i]; }
  //     }
  //     $data['username']       = 'admin_'.strtolower($set);
  //     $data['password']       = sha1(md5('123456'));
  //     $data['nama_lengkap']   = $key->kelurahan;
  //     $data['create_time']    = date('Y-m-d H:i:s');
  //     $data['create_by']      = $this->session->userdata('sesi_nama_lengkap');
  //     $data['status_aktif']   = 'Y';
  //     $data['id_user_group']  = 5;
  //     $data['id_wilayah']     = $key->id;
  //     $xss_data = $this->security->xss_clean($data);
  //     $this->db->insert('ref_users', $xss_data);
  //   }
  // }

}

?>
