<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class M_kelurahan extends CI_Model
{
  var $tbl = 'ref_kelurahan';
  var $column_search = array('rp.provinsi','rka.kabupaten_kota','rka.ibukota','rke.kecamatan','rkl.kelurahan','rkl.kd_pos');
  var $column_order = array(null,'kelurahan');
  var $order = array('id'=> 'ASC');

  function __construct()
  {
    parent::__construct();
  }

  public function get_datatables($value='')
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  public function _get_datatables_query($value='')
  {
    $this->db->select('rkl.id, rp.provinsi, rka.kabupaten_kota, rka.ibukota, rke.kecamatan, rkl.kelurahan, rkl.kd_pos');
    $this->db->join('ref_kecamatan rke','rkl.kecamatan_id=rke.id', 'LEFT');
    $this->db->join('ref_kabupaten rka','rke.kabkot_id=rka.id', 'LEFT');
    $this->db->join('ref_provinsi rp','rka.provinsi_id=rp.id', 'LEFT');
    $this->db->from('ref_kelurahan rkl');
    $i = 0;

    foreach ($this->column_search as $item) {

      if($_POST['search']['value']) {
        if($i===0) {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order'])) { // here order processing
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if(isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->select('rkl.id, rp.provinsi, rka.kabupaten_kota, rka.ibukota, rke.kecamatan, rkl.kelurahan, rkl.kd_pos');
    $this->db->join('ref_kecamatan rke','rkl.kecamatan_id=rke.id', 'LEFT');
    $this->db->join('ref_kabupaten rka','rke.kabkot_id=rka.id', 'LEFT');
    $this->db->join('ref_provinsi rp','rka.provinsi_id=rp.id', 'LEFT');
    $this->db->from('ref_kelurahan rkl');
    return $this->db->count_all_results();
  }

  public function detail($id)
  {
    $this->db->select('*');
    $this->db->from($this->tbl);
    $this->db->where('id',$id);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function tambah($data)
  {
    $this->db->insert($this->tbl,$data);
  }

  public function edit($id, $data)
  {
    $this->db->where('id',$id);
    $this->db->update($this->tbl,$data);
  }

}

?>
