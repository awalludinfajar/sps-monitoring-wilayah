<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class Data_kota_kabupaten extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->isLogin();
    $this->load->library('encrypt');
    $this->load->library('form_validation');
    $this->load->model('m_kota_kabupaten');
    // $this->load->model('m_wilayah');
  }

  public function isLogin()
  {
    if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('loginapp');
		}
  }

  public function index($value='')
  {
    $data['sub_judul_form']="Data Kota/Kabupaten";
    $data['breadcrumbs'] = array(
      array (
        'link' => 'welcome',
        'name' => 'Home'
        ),
      array (
        'link' => 'data_kota_kabupaten',
        'name' => 'Kota/Kabupaten'
        )
      );
    $data['role'] = get_role($this->session->userdata('sesi_id'));
    $this->template->load('template_frontend','v_index', $data);
  }

  public function ajax_list($value='')
  {
    $list = $this->m_kota_kabupaten->get_datatables();

    $data = array();

    $no = $_POST['start'];
    foreach ($list as $datas) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $datas->provinsi;
      $row[] = $datas->kabupaten_kota;
      $row[] = $datas->ibukota;
      $row[] = anchor('data_kota_kabupaten/edit/'.$this->encrypt->encode($datas->id), '<i class="fa fa-pencil"></i> Ubah', array('class' => 'btn btn-warning btn-sm'));
      $data[] = $row;
    }
    $output = array(
      'draw' => $_POST['draw'],
      'recordsTotal' => $this->m_kota_kabupaten->count_all(),
      'recordsFiltered' => $this->m_kota_kabupaten->count_filtered(),
      'data' => $data
    );

    echo json_encode($output);
  }

  public function add()
  {
    $valid = $this->form_validation;
    $valid->set_rules('id_provinsi','id_provinsi','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('kota_kab','kota_kab','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('ibu_kota','ibu_kota','required','xss_clean',array('required'=> '%s Harus Diisi',));

    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Kabupaten/Kota";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'data_kota_kabupaten',
          'name' => 'Kota/kabupaten'
        ),
        array (
          'link' => 'data_kota_kabupaten/add',
          'name' => 'Tambah Kota/Kabupaten'
        )
      );
      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_add', $data);
    } else {
      $id_provinsi  = htmlentities($this->input->post('nama_provinsi'), ENT_QUOTES, 'UTF-8');
      $kab_kota     = htmlentities($this->input->post('kota_kab'), ENT_QUOTES, 'UTF-8');
      $ibukota      = htmlentities($this->input->post('ibu_kota'), ENT_QUOTES, 'UTF-8');

      $data['provinsi']       = $id_provinsi;
      $data['kabupaten_kota'] = $kab_kota;
      $data['ibukota']        = $ibukota;
      $this->m_kota_kabupaten->tambah($data);
      $this->session->set_flashdata('message_sukses','Data Berhasil Ditambah');
      redirect(base_url('data_kota_kabupaten/add'),'refresh');
    }
  }

  public function edit($id)
  {
    $kode = $this->encrypt->decode($id);

    $valid = $this->form_validation;
    $valid->set_rules('id_provinsi','id_provinsi','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('kota_kab','kota_kab','required','xss_clean',array('required'=> '%s Harus Diisi',));
    $valid->set_rules('ibu_kota','ibu_kota','required','xss_clean',array('required'=> '%s Harus Diisi',));

    $id_provinsi  = htmlentities($this->input->post('nama_provinsi'), ENT_QUOTES, 'UTF-8');
    $kab_kota     = htmlentities($this->input->post('kota_kab'), ENT_QUOTES, 'UTF-8');
    $ibukota      = htmlentities($this->input->post('ibu_kota'), ENT_QUOTES, 'UTF-8');

    $dataz = $this->m_kota_kabupaten->detail($kode);
    if ($valid->run() == FALSE) {
      $data['sub_judul_form']="Data Kabupaten/Kota";
      $data['breadcrumbs'] = array(
        array (
          'link' => 'welcome',
          'name' => 'Home'
        ),
        array (
          'link' => 'data_kota_kabupaten',
          'name' => 'Kota/kabupaten'
        ),
        array (
          'link' => 'data_kota_kabupaten/edit/'.$id,
          'name' => 'Edit Kota/Kabupaten'
        )
      );
      $data['kode'] = $id;
      $data['isi'] = $dataz;
      $data['role'] = get_role($this->session->userdata('sesi_id'));
      $this->template->load('template_frontend','v_edit', $data);
    } else {
      $datax['provinsi']       = $id_provinsi;
      $datax['kabupaten_kota'] = $kab_kota;
      $datax['ibukota']        = $ibukota;

      $this->m_kota_kabupaten->edit($kode,$datax);
      $this->session->set_flashdata('message_sukses','Data Berhasil Diupdate');
      redirect(base_url('data_kota_kabupaten/edit/'.$id),'refresh');
    }

  }

  // link : http://localhost/monitoring_wilayah/data_kota_kabupaten/generate
  // public function generate($value='')
  // {
  //   $prv = $this->m_wilayah->allKabupaten();
  //   foreach ($prv as $key) {
  //     $name = explode(" ",$key->kabupaten_kota); $set = '';
  //     for ($i=0; $i < count($name); $i++) {
  //       if ($i > count($name)) { $set .= $name[$i].'_'; }
  //       else { $set .= $name[$i]; }
  //     }
  //     $data['username']       = 'admin_'.strtolower($set);
  //     $data['password']       = sha1(md5('123456'));
  //     $data['nama_lengkap']   = $key->kabupaten_kota;
  //     $data['create_time']    = date('Y-m-d H:i:s');
  //     $data['create_by']      = $this->session->userdata('sesi_nama_lengkap');
  //     $data['status_aktif']   = 'Y';
  //     $data['id_user_group']  = 3;
  //     $data['id_wilayah']     = $key->id;
  //     $xss_data = $this->security->xss_clean($data);
  //     $this->db->insert('ref_users', $xss_data);
  //   }
  // }

}

?>
