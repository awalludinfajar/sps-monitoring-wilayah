<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class Api extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->isLogin();
    $this->load->library('encrypt');
    $this->load->model('m_wilayah');
  }

  public function isLogin($value='')
  {
    if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('loginapp');
		}
  }

  public function selectProvinsi($value='')
  {
    $hasil = $this->m_wilayah->allProvinsi();
    $data = "<option value=''>Pilih</option>";
    foreach ($hasil as $res) {
      if ($value == $res->id) {
        $data .= "<option value='".$res->id."' selected >".$res->provinsi."</option>";
      } else {
        $data .= "<option value='".$res->id."'>".$res->provinsi."</option>";
      }
    }
    echo $data;
  }

  public function selectKotaKab($value='')
  {
    $hasil = $this->m_wilayah->allKabupaten($value);
    $data = "<option value=''>Pilih</option>";
    foreach ($hasil as $res) {
      $data .= "<option value='".$res->id."'>".$res->kabupaten_kota." - ".$res->ibukota."</option>";
    }
    echo $data;
  }

  public function selectKecamatan($value='')
  {
    $hasil = $this->m_wilayah->allKecamatan($value);
    $data = "<option value=''>Pilih</option>";
    foreach ($hasil as $res) {
      $data .= "<option value='".$res->id."'>".$res->kecamatan."</option>";
    }
    echo $data;
  }

  public function selectKelurahan($value='')
  {
    $hasil = $this->m_wilayah->allKelurahan($value);
    $data = "<option value=''>Pilih</option>";
    foreach ($hasil as $res) {
      $data .= "<option value='".$res->id."'>".$res->kelurahan." (".$res->kd_pos.")"."</option>";
    }
    echo $data;
  }

  public function selectRkwrga($value)
  {
    $hasil = $this->m_wilayah->allRw($value);
    $data = "<option value=''>Pilih</option>";
    foreach ($hasil as $res) {
      $data .= "<option value='".$res->id."'>".$res->no_rw.' / '.$res->nama_ketua_rw."</option>";
    }
    echo $data;
  }

  public function selectRkttg($value='')
  {
    $hasil = $this->m_wilayah->allRt($value);
    $data = "<option value=''>Pilih</option>";
    foreach ($hasil as $res) {
      $data .= "<option value='".$res->id."'>".$res->no_rt.' / '.$res->nama_ketua_rt."</option>";
    }
    echo $data;
  }

  public function selectKetuaRWSelected($value='')
  {
    $where = [$_GET['ondb'] => $value];
    $cekData = $this->m_wilayah->Wherewilayah($where);

    $hasil = $this->m_wilayah->allRw($cekData->id_kel);
    $data = "<option value=''>Pilih</option>";
    foreach ($hasil as $res) {
      if ($res->id == $cekData->id_rw) {
        $data .= "<option value='".$res->id."' selected>".$res->no_rw.' / '.$res->nama_ketua_rw."</option>";
      } else {
        $data .= "<option value='".$res->id."'>".$res->no_rw.' / '.$res->nama_ketua_rw."</option>";
      }
    }
    echo $data;
  }

  public function selectKelurahanSelected($value='')
  {
    $where = [$_GET['ondb'] => $value];
    $cekData = $this->m_wilayah->Wherewilayah($where);

    $hasil = $this->m_wilayah->allKelurahan($cekData->id_kec);
    $data = "<option value=''>Pilih</option>";
    foreach ($hasil as $res) {
      if ($res->id == $cekData->id_rkl) {
        $data .= "<option value='".$res->id."' selected>".$res->kelurahan." (".$res->kd_pos.")"."</option>";
      } else {
        $data .= "<option value='".$res->id."'>".$res->kelurahan." (".$res->kd_pos.")"."</option>";
      }
    }
    echo $data;
  }

  public function selectKecamatanSelected($value='')
  {
    $where = [$_GET['ondb'] => $value];
    $cekData = $this->m_wilayah->Wherewilayah($where);

    $hasil = $this->m_wilayah->allKecamatan($cekData->id_rka);
    $data = "<option value=''>Pilih</option>";
    foreach ($hasil as $res) {
      if ($res->id == $cekData->id_kec) {
        $data .= "<option value='".$res->id."' selected>".$res->kecamatan."</option>";
      } else {
        $data .= "<option value='".$res->id."'>".$res->kecamatan."</option>";
      }
    }
    echo $data;
  }

  public function selectKabKotaSlected($value='')
  {
    $where = [$_GET['ondb'] => $value];
    $cekData = $this->m_wilayah->Wherewilayah($where);

    $hasil = $this->m_wilayah->allKabupaten($cekData->id_prv);
    $data = "<option value=''>Pilih</option>";
    foreach ($hasil as $res) {
      if ($res->id == $cekData->id_rka) {
        $data .= "<option value='".$res->id."' selected>".$res->kabupaten_kota." - ".$res->ibukota."</option>";
      } else {
        $data .= "<option value='".$res->id."'>".$res->kabupaten_kota." - ".$res->ibukota."</option>";
      }
    }
    echo $data;
  }

  public function selectProvinsiSlected($value='')
  {
    $where = [$_GET['ondb'] => $value];
    $cekData = $this->m_wilayah->Wherewilayah($where);
    echo $this->selectProvinsi($cekData->id_prv);
  }

  // public function lookUpProvinsi($value='')
  // {
  //   $cek = $this->m_wilayah->UpProvinsi($value);
  //   echo $this->selectProvinsi($cek);
  // }
  //
  // public function lookUpKotaKab($value='')
  // {
  //   $cek = $this->m_wilayah->UpKotaKab($value);
  //   $hasil = $this->m_wilayah->allKabupaten();
  //   $data = "<option value=''>Pilih</option>";
  //   foreach ($hasil as $res) {
  //     if ($cek == $res->id) {
  //       $data .= "<option value='".$res->id."' selected>".$res->kabupaten_kota." - ".$res->ibukota."</option>";
  //     } else {
  //       $data .= "<option value='".$res->id."'>".$res->kabupaten_kota." - ".$res->ibukota."</option>";
  //     }
  //   }
  //   echo $data;
  // }
  //
  // public function lookUpKKProvinsi($value='')
  // {
  //   $cek = $this->m_wilayah->UpKotaKab($value);
  //   echo $this->selectProvinsi($cek);
  // }
  //
  // public function lookUpKecamatan($value='')
  // {
  //   $cek = $this->m_wilayah->UpKecamatan($value);
  //   $hasil = $this->m_wilayah->allKecamatan();
  //   $data = "<option value=''>Pilih</option>";
  //   foreach ($hasil as $res) {
  //     if ($cek == $res->id) {
  //       $data .= "<option value='".$res->id."' selected>".$res->kecamatan."</option>";
  //     } else {
  //       $data .= "<option value='".$res->id."'>".$res->kecamatan."</option>";
  //     }
  //   }
  //   echo $data;
  // }

}

?>
